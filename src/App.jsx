import React, { Component } from "react";
import { DragDropContext } from "react-beautiful-dnd";

import { Paper } from '@material-ui/core';

import Header from './components/header';
import List from './components/list';
import DoneList from './components/list-closed';
import PreviewItem from './components/preview-item';

import { getItems } from './services/data-generator'

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};
class App extends Component {
  state = {
    items: getItems(10),
    selected: getItems(5),
    closedCalls: [],
    previewItem: null
  };

  onPreviewItem = (item) => this.setState({ previewItem: item });

  endCall = (id) => {
    const { selected, closedCalls } = this.state;
    this.setState({
      previewItem: null,
      selected: selected.filter(i => i.id !== id),
      closedCalls: [...closedCalls, selected.find(i => i.id === id) ]
    })
  };

  saveNote = (note) => {
    const { items, selected, previewItem } = this.state;
    let item = selected.find(i => i.id === previewItem.id);
    const setNewItemsToState = (arr, newItem, key) => {
      this.setState({
        [key]: [
          { ...newItem, note },
          ...arr.filter(i => i.id !== previewItem.id)
        ]
      })
    }

    if (item) {
      setNewItemsToState(selected, item, 'selected');
    } else {
      item = items.find(i => i.id === previewItem.id);
      setNewItemsToState(items, item, 'items');
    }
  }

  id2List = {
    droppable: "items",
    droppable2: "selected",
  };

  getList = (id) => this.state[this.id2List[id]];

  onDragEnd = (result) => {
    const { source, destination } = result;

    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        this.getList(source.droppableId),
        source.index,
        destination.index
      );

      let state = { items };
      if (source.droppableId === "droppable2") {
        state = { selected: items };
      }

      this.setState(state);
    } else {
      const result = move(
        this.getList(source.droppableId),
        this.getList(destination.droppableId),
        source,
        destination
      );

      this.setState({
        items: result.droppable,
        selected: result.droppable2,
      });
    }
  };

  render() {
    const { previewItem, closedCalls, items, selected } = this.state;

    return (
      <>
      <Header />
      <Paper className="wrapper">
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div className="list-column">
            <h2>Вхідні виклики</h2>
            <List 
              id="droppable"
              items={items}
            />
          </div>
          <div className="list-column in-progress">
            <h2>Черга опрацювань</h2>
            <List 
              id="droppable2"
              items={selected}
              onPreviewItem={this.onPreviewItem}
              activeItem={previewItem}
            />
          </div>
          </DragDropContext>
          {previewItem && <PreviewItem item={previewItem} saveNote={this.saveNote} endCall={this.endCall} />}
          {
            closedCalls.length > 0 && (
              <div className="list-column done">
                <h2>Опрацьовані виклики</h2>
                <DoneList items={closedCalls} />
              </div>
            )
          }
      </Paper>
      </>
    );
  }
}

export default App;

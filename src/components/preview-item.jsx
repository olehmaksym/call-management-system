import React, { useState, useEffect } from 'react'
import { Button, Fab } from '@material-ui/core';
import CallEndIcon from '@material-ui/icons/CallEnd';
import MicOffIcon from '@material-ui/icons/MicOff';
import MusicNoteIcon from '@material-ui/icons/MusicNote';

const PreviewItem = ({ item, saveNote, endCall }) => {
  const [note, setNote] = useState(item.note ?? '');
  useEffect(() => {
    setNote(item.note ?? '')
  }, [item]);

  return (
    <div className="list-column preview">
      <h2>Деталі виклику</h2>
      <p>Абонент: <b>{item.name}</b></p>
      <p>Телефон: <b>{item.phone}</b></p>
      <p>Нотатка до виклику:</p>
      <textarea 
        name="details"
        cols="30"
        rows="8"
        value={note}
        onChange={e => setNote(e.target.value)}
      />
      <Button
        variant="contained"
        onClick={() => saveNote(note)}
      >
          Зберегти нотатку
      </Button>
      <div className="row actions">
        <Fab color="secondary" onClick={() => endCall(item.id)} title="Завершити">
          <CallEndIcon/>
        </Fab>
        <Fab title="Вимкнути мікрофон">
          <MicOffIcon/>
        </Fab>
        <Fab title="Записати розмову">
          <MusicNoteIcon/>
        </Fab>
      </div>
    </div>
  )
}

export default PreviewItem;

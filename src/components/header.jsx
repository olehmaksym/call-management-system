import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

const styles = (theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    cursor: 'default'
  },
  toolBar: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  heading: {
    color: 'white'
  }
});

const Header = ({ classes }) => (
  <AppBar position="fixed" className={classes.appBar}>
    <Toolbar className={classes.toolBar}>
      <Typography className={classes.heading} variant="h6" noWrap>
        CMS
      </Typography>
    </Toolbar>
  </AppBar>
);

export default withStyles(styles)(Header);

import React from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import {
  ListItem, 
  ListItemText,
  ListItemAvatar,
  Avatar
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';

const grid = 10;

const getItemStyle = (isDragging, draggableStyle, itemId, activeItem) => ({
  userSelect: "none",
  margin: `0 0 ${grid}px 0`,
  background: isDragging 
    ? "lightgreen" 
    : itemId === activeItem?.id ? "yellow": "white",
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "lightgrey",
  padding: grid,
  width: 250,
});

const DList = ({ id, items, onPreviewItem, activeItem }) => {
  return (
    <Droppable droppableId={id}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          style={getListStyle(snapshot.isDraggingOver)}
        >
          {items.map((item, index) => (
            <Draggable key={item.id} draggableId={item.id} index={index}>
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  style={getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style,
                    item.id,
                    activeItem
                  )}
                  onClick={() => onPreviewItem?.(item)}
                >
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar><PersonIcon /></Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={item.name} secondary={item.phone} />
                  </ListItem>
                </div>
              )}
            </Draggable>
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  )
};

export default DList;

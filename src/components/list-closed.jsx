import React from "react";
import {
  ListItem, 
  ListItemText,
  ListItemAvatar,
  Avatar
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';

const grid = 10;

const getItemStyle = () => ({
  userSelect: "none",
  margin: `0 0 ${grid}px 0`,
  background: "white"
});

const getListStyle = () => ({
  padding: grid,
  background: "lightgrey",
  width: 250,
});

const DList = ({ items }) => {
  return (
    <div style={getListStyle()}>
      {items.map((item) => (
        <div
          key={item.id}
          style={getItemStyle()}
        >
          <ListItem className="list-item">
            <ListItemAvatar>
              <Avatar><PersonIcon /></Avatar>
            </ListItemAvatar>
            <ListItemText primary={item.name} secondary={item.phone} />
            <p className="note">{item?.note}</p>
          </ListItem>
        </div>
      ))}
    </div>
  )
};

export default DList;

const names = [
  'Іван',
  'Оля',
  'Сергій',
  'Микола',
  'Оксана',
  'Ліля',
  'Борис',
  'Тетяна',
  'Володимир',
  'Катерина'
]

const generatePhone = () => Math.random().toString().slice(2,11);

export const getItems = (count) =>
  Array.from({ length: count }, (v, k) => k).map((k) => {
    const p = generatePhone();
    return {
      id: `call-${p.slice(2,7)}-${k}`,
      name: names[p.slice(1,2)],
      phone: `+380 (${p.slice(0,2)}) ${p.slice(2,5)} ${p.slice(5,7)} ${p.slice(7,9)}`
    }
  });
